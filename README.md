# MonoGlyph

MonoGlyph is an effort to easily provide monospaced bitmap fonts as C headers.

Instead of attempting to provide a single font with coverage for the entire Unicode, fonts containing different character sets will be provided separatelly. This means this project is also a database for monospaced bitmap fonts in C, and you can contribute with fonts with different styles and for different languages.

The fonts are provided as generated headers, located in `/fonts`. The header `/fonts/monoglyph.h`, which contains the definition for `struct glyph` and `struct glyph_set`, is included by the header of each font.

## Contributing

**Contributions are very welcome!** Please, contritube and let's build a great database of monospaced fonts that can be easily used from C language!

To contribute with a new font, you must create an image for the font and a text file containing the sequence of characters in the image (check the font `simple1_5x8x8` as example).

The name of the files must follow this pattern:

- For the image: `[lower_case_font_name]_[baseline]x[glyph-width]x[glyph-height].png`.
- For the text: `[lower_case_font_name]_[baseline]x[glyph-width]x[glyph-height]` (without any extension).

To contribute to this project, you must have the legal right to provide the font that you are providing (for example, the font was created by you or the license is compatible with ZLIB).

To submit a new font, you can make a pull request or open an issue with the image attached, the text file, and the source of the font: you or a link to the origin. Contributors will have their name added to `CONTRIBUTORS.md`.

You can use the [charts of the Unicode standard](https://www.unicode.org/charts) as a reference to check the characters contained in each language.

### Image

- Characters are organized in rows and columns.
- The number of columns must be 16, and after reaching the end, place the next characters in the next row.
- Characters must fit in the same width and height, that's why it's called monospaced.
- The background must be transparent black, for standardization.
- The characters should be preferably opaque white, but with different colors are also accepted.

Note that, whatever the format of the image (RGBA, RGB, indexed palette), the output in the header is in RGBA format.

### Text File

- The first line must have, separated by comma: image width, image height, glyph baseline, glyph width, glyph height.
- Starting on the second line:
  - The text file should contain the same character set that the bitmap contains, in the same order.
  - You can use the `newline` character to organize the text by columns and rows, like in the image, for better visualization.
  - Non-printable characters, such as `space` and `tab`, must not be added. The only non-printable character that can be added is `newline`, used for organizing the text file.

For example, for an image with width `128` and height `64`, glyph with baseline `6`, width `8` and height `8`:

```
128, 64, 6, 8, 8
…
```

## Generating Headers

Use the tool `generate`, that will use the text file and the PNG image to create the C header:

```
generate [PATH] [FONT NAME]

Example:
generate ./fonts/ simple1_5x8x8
```

The font name must respect the same rules of naming variables in C. The generator assumes that there is a text file with the font name, an image file with the font name and extension `.png`, and will generate a C header with the font name and extension `.h`.

At the end of the generated header, you can find a structure `struct glyph_set`, containing information about the image size, a pointer to the image, the baseline and size of the glyphs, the total of glyphs, and a pointer to access the glyphs.

Each glyph structure `struct glyph` contains the codepoint of the character, and the column and row in which the character is located in the image. Then, the exact top-left location of the character in the image can be found multiplying the row by the glyph height and the column by the glyph width.

## How To Use

Include the font header in one of your source files. Since the variables in the header are declared as `static`, the source file including the header will have its own copy of the variables.

The slow way of accessing the glyphs is parsing the array in the `struct glyph_set`. The fast way is using the codepoint of the glyph that you want as index:

```c
uint32_t x = 0;
uint32_t codepoint = 0;
char *s = /* My string */;
char *iterator = s;
iterator = utf8_decode(iterator, &codepoint);
while (codepoint) {
	if (codepoint != 32) { /* Check if it's not space */
		struct glyph *glyph = &fontname_glyph_set.glyphs[codepoint - fontname_glyph_set.lowest_codepoint];
		uint32_t top = glyph->row * fontname_glyph_set.glyph.height;
		uint32_t left = glyph->col * fontname_glyph_set.glyph.width;
		/* Draw character */
	}
	x = x + fontname_glyph_set.glyph.width;
	iterator = utf8_decode(iterator, &codepoint);
}
```

MonoGlyph is not responsible for rendering or text layout. There are many specific user cases for rendering, such as OpenGL, DirectX, Vulkan and other software-based cases. As for text layout, implementing yourself for monospaced fonts should not be complex, since each character has the same size.

## License

Copyright © 2018 Felipe Ferreira da Silva

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use of
this software.

Permission is granted to anyone to use this software for any purpose, including
commercial applications, and to alter it and redistribute it freely, subject to
the following restrictions:

  1. The origin of this software must not be misrepresented; you must not claim
     that you wrote the original software. If you use this software in a
     product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
