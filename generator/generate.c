/*
Copyright © 2018 Felipe Ferreira da Silva

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use of
this software.

Permission is granted to anyone to use this software for any purpose, including
commercial applications, and to alter it and redistribute it freely, subject to
the following restrictions:

  1. The origin of this software must not be misrepresented; you must not claim
     that you wrote the original software. If you use this software in a
     product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

Compiling using GCC:
  gcc generate.c -o generate -I ../fonts
*/

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>
#include <monoglyph.h>
#define CUTE_PNG_IMPLEMENTATION
#include "cute_png.h"

#define PATH_MAX_LENGTH 5012
#define FONT_NAME_MAX_LENGTH 128
#define MAX_CODEPOINTS_PER_FONT 2048
#define INPUT_TEXT_MAX_SIZE (MAX_CODEPOINTS_PER_FONT * 4)

struct application {
	char path[PATH_MAX_LENGTH];
	char font_name[FONT_NAME_MAX_LENGTH];
	char font_name_upper[FONT_NAME_MAX_LENGTH];
	FILE *output;
	char text[INPUT_TEXT_MAX_SIZE];
	struct glyph_set glyph_set;
	struct glyph glyphs[MAX_CODEPOINTS_PER_FONT];
};

#if defined(_WIN32) || defined (_WIN64)
char path_separator = '\\';
#else
char path_separator = '/';
#endif

struct application application = {0};

#define MAXUNICODE 0x10FFFF
uint32_t utf8_limits[] = {0xFF, 0x7F, 0x7FF, 0xFFFF};

static char *utf8_decode(char *o, uint32_t *val)
{
	unsigned char *s = (unsigned char *)o;
	uint32_t c = s[0];
	uint32_t res = 0;
	if (c < 0x80) {
		res = c;
	} else {
		int32_t count = 0;
		while (c & 0x40) {
			int32_t cc = s[++count];
			if ((cc & 0xC0) != 0x80) {
				return NULL;
			}
			res = (res << 6) | (cc & 0x3F);
			c <<= 1;
		}
		res = res | ((c & 0x7F) << (count * 5));
		if (count > 3 || res > MAXUNICODE || res <= utf8_limits[count]) {
			return NULL;
		}
		s = s + count;
	}
	if (val) {
		*val = res;
	}
	return (char *)s + 1;
}

static void utf8_encode(char *out, uint32_t codepoint)
{
	if (codepoint <= 0x7F) {
		out[0] = (char)codepoint;
		out[1] = 0;
	} else if (codepoint <= 0x07FF) {
		out[0] = (char)(((codepoint >> 6) & 0x1F) | 0xC0);
		out[1] = (char)(((codepoint >> 0) & 0x3F) | 0x80);
		out[2] = 0;
	} else if (codepoint <= 0xFFFF) {
		out[0] = (char)(((codepoint >> 12) & 0x0F) | 0xE0);
		out[1] = (char)(((codepoint >>  6) & 0x3F) | 0x80);
		out[2] = (char)(((codepoint >>  0) & 0x3F) | 0x80);
		out[3] = 0;
	} else if (codepoint <= 0x10FFFF) {
		out[0] = (char)(((codepoint >> 18) & 0x07) | 0xF0);
		out[1] = (char)(((codepoint >> 12) & 0x3F) | 0x80);
		out[2] = (char)(((codepoint >>  6) & 0x3F) | 0x80);
		out[3] = (char)(((codepoint >>  0) & 0x3F) | 0x80);
		out[4] = 0;
	} else { 
		out[0] = (char)0xEF;  
		out[1] = (char)0xBF;
		out[2] = (char)0xBD;
		out[3] = 0;
	}
}

static void print_glyphs(void)
{
	uint32_t codepoint = 0;
	uint32_t i = 0;
	uint32_t unused = 0;
	char *pos = application.text;
	pos = utf8_decode(pos, &codepoint);
	pos = utf8_decode(pos, &codepoint);
	application.glyph_set.lowest_codepoint = codepoint;
	application.glyph_set.highest_codepoint = codepoint;
	while (codepoint) {
		if (codepoint != 10) {
			if (codepoint < application.glyph_set.lowest_codepoint) {
				application.glyph_set.lowest_codepoint = codepoint;
			}
			if (codepoint > application.glyph_set.highest_codepoint) {
				application.glyph_set.highest_codepoint = codepoint;
			}
		}
		pos = utf8_decode(pos, &codepoint);
	}
	pos = application.text;
	pos = utf8_decode(pos, &codepoint);
	pos = utf8_decode(pos, &codepoint);
	while (codepoint) {
		if (codepoint != 10) {
			char utf8[5] = {0};
			struct glyph *glyph = &application.glyphs[codepoint - application.glyph_set.lowest_codepoint];
			glyph->codepoint = codepoint;
			glyph->row = i / 16;
			glyph->col = i % 16;
			application.glyph_set.count = application.glyph_set.count + 1;
			i = i + 1;
			utf8_encode(utf8, glyph->codepoint);
		}
		pos = utf8_decode(pos, &codepoint);
	}
	fprintf(application.output, "static struct glyph %s_glyphs[] = {\n", application.font_name);
	i = application.glyph_set.lowest_codepoint;
	while (i <= application.glyph_set.highest_codepoint) {
		struct glyph *glyph = &application.glyphs[i - application.glyph_set.lowest_codepoint];
		if (i - application.glyph_set.lowest_codepoint > 0) {
			fprintf(application.output, "\t},\n");
		}
		fprintf(application.output, "\t{\n");
		if (glyph->codepoint) {
			char utf8[5] = {0};
			utf8_encode(utf8, glyph->codepoint);
			fprintf(application.output, "\t\t.codepoint = %u, /* \"%s\" */\n", glyph->codepoint, utf8);
			fprintf(application.output, "\t\t.col = %u,\n", glyph->col);
			fprintf(application.output, "\t\t.row = %u\n", glyph->row);
		} else {
			unused = unused + 1;
			fprintf(application.output, "\t\t.codepoint = 0 /* Unused */\n");
		}
		i = i + 1;
	}
	fprintf(application.output, "\t}\n");
	fprintf(application.output, "};\n\n");
	fprintf(application.output, "#define %s_IMAGE_WIDTH %u\n", application.font_name_upper, application.glyph_set.image.width);
	fprintf(application.output, "#define %s_IMAGE_HEIGHT %u\n\n", application.font_name_upper, application.glyph_set.image.height);
	fprintf(application.output, "static struct glyph_set %s_glyph_set = {\n", application.font_name);
	fprintf(application.output, "\t.image = {\n");
	fprintf(application.output, "\t\t.width = %s_IMAGE_WIDTH,\n", application.font_name_upper);
	fprintf(application.output, "\t\t.height = %s_IMAGE_HEIGHT,\n", application.font_name_upper);
	fprintf(application.output, "\t\t.pixels = %s_pixels\n", application.font_name);
	fprintf(application.output, "\t},\n");
	fprintf(application.output, "\t.glyph = {\n");
	fprintf(application.output, "\t\t.baseline = %u,\n", application.glyph_set.glyph.baseline);
	fprintf(application.output, "\t\t.width = %u,\n", application.glyph_set.glyph.width);
	fprintf(application.output, "\t\t.height = %u\n", application.glyph_set.glyph.height);
	fprintf(application.output, "\t},\n");
	fprintf(application.output, "\t.lowest_codepoint = %u,\n", application.glyph_set.lowest_codepoint);
	fprintf(application.output, "\t.highest_codepoint = %u,\n", application.glyph_set.highest_codepoint);
	fprintf(application.output, "\t.count = %u,\n", application.glyph_set.count);
	fprintf(application.output, "\t.glyphs = %s_glyphs\n", application.font_name);
	fprintf(application.output, "};\n\n");
	printf("\nPrinted %u codepoints. There are %u unused codepoints\n", application.glyph_set.count, unused);
}

static bool print_image(void)
{
	bool success = true;
	uint32_t i = 0;
	uint32_t col = 0;
	char input_png_path[PATH_MAX_LENGTH] = {0};
	cp_image_t img;
	strncat(input_png_path, application.path, PATH_MAX_LENGTH - 1);
	strncat(input_png_path, ".png", PATH_MAX_LENGTH - 1);
	img = cp_load_png(input_png_path);
	if (!img.pix) {
		success = false;
		goto done;
	}
	fprintf(application.output, "static uint8_t %s_pixels[] = {", application.font_name);
	while (i < (uint32_t)(img.w * img.h)) {
		if (i > 0) {
			fputc(',', application.output);
			if (col < 3) {
				fputc(' ', application.output);
			} else {
				col = 0;
			}
		}
		if (col == 0) {
			fputc('\n', application.output);
			fputc('\t', application.output);
		}
		fprintf(application.output, "0x%02X, 0x%02X, 0x%02X, 0x%02X", img.pix[i].r, img.pix[i].g, img.pix[i].b, img.pix[i].a);
		col = col + 1;
		i = i + 1;
	}
	fprintf(application.output, "\n};\n\n");
	free(img.pix);
done:
	return success;
}

static bool open_output(void)
{
	bool success = true;
	char output_path[PATH_MAX_LENGTH] = {0};
	strncat(output_path, application.path, PATH_MAX_LENGTH - 1);
	strncat(output_path, ".h", PATH_MAX_LENGTH - 1);
	application.output = fopen(output_path, "w");
	if (!application.output) {
		success = false;
	}
	return success;
}

static bool parse_text_file(void)
{
	bool success = true;
	char input_text_path[PATH_MAX_LENGTH] = {0};
	FILE *input_text;
	uint32_t text_start = 0;
	int32_t c = 0;
	uint32_t i = 0;
	strncat(input_text_path, application.path, PATH_MAX_LENGTH - 1);
	input_text = fopen(input_text_path, "rb");
	if (!input_text) {
		success = false;
		goto done;
	}
	fscanf(input_text, "%u, %u, %u, %u, %u",
		&application.glyph_set.image.width,
		&application.glyph_set.image.height,
		&application.glyph_set.glyph.baseline,
		&application.glyph_set.glyph.width,
		&application.glyph_set.glyph.height);
	text_start = ftell(input_text);
	fseek(input_text, 0, SEEK_END);
	fseek(input_text, text_start, SEEK_SET);
	c = fgetc(input_text);
	while (c != EOF) {
		application.text[i] = c;
		c = fgetc(input_text);
		i = i + 1;
	}
	fclose(input_text);
done:
	return success;
}

static void make_font_name_upper(void)
{
	uint32_t i = 0;
	while (application.font_name_upper[i] != '\0') {
		application.font_name_upper[i] = toupper(application.font_name_upper[i]);
		i = i + 1;
	}
}

int main(int argc, char **args)
{
	if (argc == 2) {
		strncpy(application.path, args[1], PATH_MAX_LENGTH);
		if (strrchr(application.path, path_separator)) {
			strncpy(application.font_name, strrchr(application.path, path_separator) + 1, FONT_NAME_MAX_LENGTH);
			strncpy(application.font_name_upper, strrchr(application.path, path_separator) + 1, FONT_NAME_MAX_LENGTH);
			make_font_name_upper();
		}
	}
	if (argc != 2 || application.path[0] == '\0' || application.font_name[0] == '\0'){
		puts("Usage:");
		puts("  generate /path/to/font-name");
		puts("");
		goto done;
	}
	printf("Your entries are:\n"
		"       Text file: %s\n"
		"      Image file: %s.png\n"
		"       Font name: %s\n",
		application.path,
		application.path,
		application.font_name);
	if (!parse_text_file()) {
		printf("Failed to read text file.\n");
		goto done;
	}
	if (!open_output()) {
		printf("Failed to open output file.\n");
		goto done;
	}
	fprintf(application.output, "#ifndef %s_GLYPHS_H\n", application.font_name_upper);
	fprintf(application.output, "#define %s_GLYPHS_H\n\n", application.font_name_upper);
	fprintf(application.output, "#include \"monoglyph.h\"\n\n");
	if (!print_image()) {
		printf("Failed to open PNG file.\n");
		goto done;
	}
	print_glyphs();
	fprintf(application.output, "#endif\n");
	fclose(application.output);
done:
	return EXIT_SUCCESS;
}
